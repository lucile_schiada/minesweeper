var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

var User = require('./server/models/user.js');
var createGame = require('./server/actions/create-game.js');

(function setupSocketIo(){
    io.on('connection', onConnection);

    function onConnection(socket){
        socket.user = new User(socket.id);
        socket.on('createGame', onCreateGame);
        socket.on('revealed', onTileRevealed);
        socket.on('annotated', onTileAnnotated);
        socket.on('disconnect', onDisconnect);
        socket.on('name', onName);
        socket.on('invitation', onInvitation);
        socket.on('response', onResponse);
        socket.emit('otherUsers', io.sockets.sockets
            .filter(isOtherSocket)
            .map(toUser));

        function onCreateGame(options){
            socket.user.game = createGame(options);
            socket.emit('game', socket.user.game);
            socket.broadcast.emit('otherUser', socket.user);
        }

        function onTileRevealed(coordinates){
            socket.user.game.reveal(coordinates);
            socket.broadcast.emit('revealed', {
                socketId: socket.user.socketId,
                coordinates: coordinates
            });
        }

        function onTileAnnotated(coordinates){
            socket.user.game.annotate(coordinates);
            socket.broadcast.emit('annotated', {
                socketId: socket.user.socketId,
                coordinates: coordinates
            });
        }

        function onDisconnect(){
            socket.broadcast.emit('disconnected', socket.user.socketId);
        }

        function onName(name){
            socket.user.name = name;
            socket.broadcast.emit('otherUser', socket.user);
        }

        function onInvitation(invitation){
            var guestSocket = io.sockets.connected[invitation.guestSocketId];
            guestSocket.emit('invitation', { hostSocketId: socket.user.socketId });
        }
        
        function onResponse(response){
            var hostSocket = io.sockets.connected[response.hostSocketId];
            hostSocket.emit('response', { guestSocketId: socket.user.socketId });
        }

        function isOtherSocket(otherSocket){
            return otherSocket !== socket;
        }

        function toUser(otherSocket){
            return otherSocket.user;
        }
    }
}());


app.use(express.static('public'));

var DEFAULT_PORT = 3000;
var port = process.env.PORT || DEFAULT_PORT;

server.listen(port);


