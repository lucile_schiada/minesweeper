var Game = require('../models/game.js');
var Grid = require('../models/grid.js');

var DEFAULT_DIFFICULTY = 'Easy';

module.exports = function createGame(options){
    options = options || {};
    options.difficulty = options.difficulty || DEFAULT_DIFFICULTY;
    return new Game(options.difficulty, createGrid(options));
};

function createGrid(options){
    switch(options.difficulty){
        case 'Easy':
            return new Grid(9, 9, 0.123456789);
        case 'Medium':
            return new Grid(16, 16, 0.15625);
        default:
            return new Grid(30, 16);
    }
}
