function Game(difficulty, grid){
    this._difficulty = difficulty;
    this._grid = grid;
}

Game.prototype = {
    _difficulty: undefined,

    _grid: undefined,
    get grid(){
        return this._grid;
    },

    _startTime: undefined,
    get startTime(){
        return this._startTime;
    },

    _lost: false,
    get lost(){
        return this._lost;
    }
};

Game.prototype.reveal = function(coordinates){
    var tile = this._grid.getTile(coordinates);
    if(this._lost)
        return false;
    if(tile.flag)
        return false;
    if(tile.mine)
        this.gameLost();

    if(!this._startTime)
        this._startTime = Date.now();

    tile.revealed = true;
    if(!this._lost && this._grid.getNeighboringMinesCount(tile.coordinates) == 0)
        this._grid.getUnrevealedNeighbors(tile.coordinates)
            .map(toCoordinates)
            .forEach(this.reveal, this);

    return true;

    function toCoordinates(tile){
        return tile.coordinates;
    }
};

Game.prototype.gameLost = function(){
    this._lost = true;
};

Game.prototype.annotate = function(coordinates){
    var tile = this._grid.getTile(coordinates);
    var _this = this;
    if(tile.revealed || this._lost)
        return false;
    if(tile.flag)
        return placeQuestionMark();
    if(tile.questionMark)
        return removeFlagAndQuestionMark();
    if(!canPlaceFlag())
        return false;
    return placeFlag();

    function placeQuestionMark(){
        tile.flag = false;
        tile.questionMark = true;
        return true;
    }

    function removeFlagAndQuestionMark(){
        tile.flag = false;
        tile.questionMark = false;
        return true;
    }

    function canPlaceFlag(){
        return _this._grid.countFlags() < _this._grid.minesCount;
    }

    function placeFlag(){
        tile.flag = true;
        tile.questionMark = false;
        return true;
    }

};
module.exports = Game;
