function User(socketId){
    this.socketId = socketId;
}

User.prototype = {
    socketId: undefined,
    game: undefined
};

module.exports = User;

