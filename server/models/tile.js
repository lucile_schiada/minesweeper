function Tile(x, y){
    this.coordinates = {
        x: x,
        y: y
    };
}

Tile.prototype = {
    mine: false,
    revealed: false,
    flag: false,
    questionMark: false,
    coordinates: undefined
};
module.exports = Tile;