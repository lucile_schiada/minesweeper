var Tile = require('./tile.js');

var DEFAULT_MINES_RATIO = 0.20625;

function Grid(width, height, minesRatio){
    minesRatio = minesRatio || DEFAULT_MINES_RATIO;
    this._width = width;
    this._height = height;
    this._minesCount = Math.round(width * height * minesRatio);

    this._createTiles()
        ._setMines();
}

Grid.prototype = {
    _width: undefined,
    _height: undefined,

    _minesCount: undefined,
    get minesCount(){
        return this._minesCount;
    },

    _tiles: undefined,
    get tiles(){
        return this._tiles;
    }
};

Grid.prototype._createTiles = function(){
    this._tiles = [];
    for(var x = 0; x < this._width; x++){
        this._tiles[x] = [];
        for(var y = 0; y < this._height; y++){
            this._tiles[x][y] = new Tile(x, y);
        }
    }
    return this;
};

Grid.prototype._setMines = function(){
    var tiles = this.allTiles();
    for(var i = 0; i < this._minesCount; i++){
        var index = Math.round(Math.random() * (tiles.length - 1));
        var tile = tiles.splice(index, 1)[0];
        tile.mine = true;
    }
    return this;
};

Grid.prototype.allTiles = function(){
    return this._tiles.reduce(concatColumns, []);

    function concatColumns(tiles, column){
        return tiles.concat(column);
    }
};

Grid.prototype.getNeighboringMinesCount = function(coordinates){
    return this.getNeighbors(coordinates).reduce(countMines, 0);

    function countMines(minesCount, tile){
        return (tile.mine)? ++minesCount : minesCount;
    }
};

Grid.prototype.getUnrevealedNeighbors = function(coordinates){
    return this.getNeighbors(coordinates).filter(isUnrevealed);

    function isUnrevealed(tile){
        return !tile.revealed;
    }
};

Grid.prototype.getNeighbors = function(coordinates){
    return this.getNeighborsCoordinates(coordinates).map(this.getTile, this);
};

Grid.prototype.getTile = function(coordinates){
    return this._tiles[coordinates.x][coordinates.y];
};

Grid.prototype.getNeighborsCoordinates = function(coordinates){
    return [
        { x: coordinates.x - 1, y: coordinates.y - 1 },
        { x: coordinates.x - 1, y: coordinates.y + 1 },
        { x: coordinates.x - 1, y: coordinates.y },
        { x: coordinates.x, y: coordinates.y - 1 },
        { x: coordinates.x, y: coordinates.y + 1 },
        { x: coordinates.x + 1, y: coordinates.y - 1 },
        { x: coordinates.x + 1, y: coordinates.y + 1 },
        { x: coordinates.x + 1, y: coordinates.y }
    ].filter(this.isInGrid, this);
};

Grid.prototype.isInGrid = function(coordinates){
    return (coordinates.x < this._width
    && coordinates.x >= 0
    && coordinates.y < this._height
    && coordinates.y >= 0);
};

Grid.prototype.countFlags = function(){
    return this.allTiles()
        .filter(hasFlag)
        .length;

    function hasFlag(tile){
        return tile.flag;
    }
};

module.exports = Grid;