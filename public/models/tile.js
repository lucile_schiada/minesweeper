angular.module('minesweeper')
    .factory('Tile', [
        function(){
            function Tile(tileObject){
                this.mine = tileObject.mine;
                this.revealed = tileObject.revealed;
                this.flag = tileObject.flag;
                this.questionMark = tileObject.questionMark;
                this.coordinates = tileObject.coordinates;
            }

            Tile.prototype = {
                mine: false,
                revealed: false,
                flag: false,
                questionMark: false,
                coordinates: undefined
            };

            return Tile;
        }]);






