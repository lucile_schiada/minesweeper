angular.module('minesweeper')
    .factory('User', [
        'Game',
        function(Game){
            function User(userObject){
                this.socketId = userObject.socketId;
                this.name = userObject.name;
                if(userObject.game)
                    this.game = new Game(userObject.game);
            }

            User.prototype = {
                name: undefined,
                socketId: undefined,
                game: undefined
            };
            return User;
        }]);

