angular.module('minesweeper')
    .factory('Grid', [
        'Tile',
        function(Tile){

            function Grid(gridObject){
                this._width = gridObject._width;
                this._height = gridObject._height;
                this._minesCount = gridObject._minesCount;
                this._tiles = gridObject._tiles;

                this._createTiles();
            }

            Grid.prototype = {
                _width: undefined,
                _height: undefined,

                _minesCount: undefined,
                get minesCount(){
                    return this._minesCount;
                },

                _tiles: undefined,
                get tiles(){
                    return this._tiles;
                }
            };

            Grid.prototype._createTiles = function(){
                for(var x = 0; x < this._width; x++)
                    for(var y = 0; y < this._height; y++)
                        this._tiles[x][y] = new Tile(this._tiles[x][y]);
                return this;
            };

            Grid.prototype.getNeighboringMinesCount = function(coordinates){
                return this.getNeighbors(coordinates).reduce(countMines, 0);

                function countMines(minesCount, tile){
                    return (tile.mine)? ++minesCount : minesCount;
                }
            };

            Grid.prototype.getUnrevealedNeighbors = function(coordinates){
                return this.getNeighbors(coordinates).filter(isUnrevealed);

                function isUnrevealed(tile){
                    return !tile.revealed;
                }
            };

            Grid.prototype.getNeighbors = function(coordinates){
                return this.getNeighborsCoordinates(coordinates).map(this.getTile, this);
            };

            Grid.prototype.getTile = function(coordinates){
                return this._tiles[coordinates.x][coordinates.y];
            };

            Grid.prototype.getNeighborsCoordinates = function(coordinates){
                return [
                    { x: coordinates.x - 1, y: coordinates.y - 1 },
                    { x: coordinates.x - 1, y: coordinates.y + 1 },
                    { x: coordinates.x - 1, y: coordinates.y },
                    { x: coordinates.x, y: coordinates.y - 1 },
                    { x: coordinates.x, y: coordinates.y + 1 },
                    { x: coordinates.x + 1, y: coordinates.y - 1 },
                    { x: coordinates.x + 1, y: coordinates.y + 1 },
                    { x: coordinates.x + 1, y: coordinates.y }
                ].filter(this.isInGrid, this);
            };

            Grid.prototype.isInGrid = function(coordinates){
                return (coordinates.x < this._width
                && coordinates.x >= 0
                && coordinates.y < this._height
                && coordinates.y >= 0);
            };

            Grid.prototype.countFlags = function(){
                return this.allTiles()
                    .filter(hasFlag)
                    .length;

                function hasFlag(tile){
                    return tile.flag;
                }
            };

            Grid.prototype.countRevealedTiles = function(){
                return this.allTiles()
                    .filter(isRevealed)
                    .length;

                function isRevealed(tile){
                    return tile.revealed;
                }
            };

            Grid.prototype.allTiles = function(){
                return this._tiles.reduce(concatColumns, []);

                function concatColumns(tiles, column){
                    return tiles.concat(column);
                }
            };

            return Grid;
        }]);