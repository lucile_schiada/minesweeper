angular.module('minesweeper')
    .service('UsersService', [
        '$rootScope',
        'User',
        'socket',

        function($rootScope, User, socket){
            (function setupUsersMessages(UsersService){
                var users = [];

                socket.on('otherUsers', setUsers);
                socket.on('otherUser', createOrUpdateUser);
                Object.defineProperty(UsersService, 'users', {
                    get: function(){
                        return users;
                    }
                });

                function setUsers(userObjects){
                    users = userObjects.map(toUser);

                    $rootScope.$digest();

                    function toUser(userObject){
                        return new User(userObject);
                    }
                }

                function createOrUpdateUser(userObject){
                    var user = new User(userObject);
                    var userIndex = UsersService.getUserIndex(user.socketId);
                    if(userIndex != -1)
                        users[userIndex] = user;
                    else
                        users.push(user);

                    $rootScope.$digest();
                }

            }(this));

            (function setupUsersQueries(UsersService){
                UsersService.getUser = function(socketId){
                    var userIndex = UsersService.getUserIndex(socketId);
                    return (userIndex != -1)? UsersService.users[userIndex] : null;
                };

                UsersService.removeUser = function(socketId){
                    var userIndex = UsersService.getUserIndex(socketId);
                    if(userIndex != -1)
                        UsersService.users.splice(userIndex, 1);
                };

                UsersService.getUserIndex = function(socketId){
                    for(var i = 0; i < UsersService.users.length; i++)
                        if(UsersService.users[i].socketId == socketId)
                            return i;
                    return -1;
                };
            }(this));

        }]);
