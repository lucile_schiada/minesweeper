angular.module('minesweeper', [
    'ngCookies',
    'ez.rightClick',
    'btford.socket-io',
    'timer'
])
    .factory('socket', [
        'socketFactory',

        function(socketFactory){
            var socket = socketFactory();
            socket.forward(['game', 'revealed', 'annotated', 'disconnected', 'invitation', 'response']);
            return socket;
        }
    ])
    .controller('AppController', [
        '$scope',
        '$cookies',
        'socket',
        'Game',
        'UsersService',

        function($scope, $cookies, socket, Game, UsersService){
            var COOKIES_DIFFICULTY_KEY = 'difficulty';
            var DEFAULT_DIFFICULTY = 'Easy';
            $scope.difficulty = $cookies.get(COOKIES_DIFFICULTY_KEY) || DEFAULT_DIFFICULTY;
            $scope.saveDifficulty = function(){
                $cookies.put(COOKIES_DIFFICULTY_KEY, $scope.difficulty);
            };

            $scope.promptName = function(){
                do
                    $scope.name = prompt('Name : ');
                while(!$scope.name);
                socket.emit('name', $scope.name);
            };

            $scope.reset = function(){
                socket.emit('createGame', { difficulty: $scope.difficulty });
            };

            $scope.$on('socket:game', function(event, gameObject){
                $scope.game = new Game(gameObject);

            });

            $scope.$on('socket:revealed', function(event, data){
                var user = UsersService.getUser(data.socketId);
                user.game.reveal(data.coordinates);
            });

            $scope.$on('socket:annotated', function(event, data){
                var user = UsersService.getUser(data.socketId);
                user.game.annotate(data.coordinates);
            });

            $scope.$on('socket:disconnected', function(event, socketId){
                UsersService.removeUser(socketId);
            });

            $scope.sendInvitation = function(guest){
                socket.emit('invitation', { guestSocketId: guest.socketId });
            };

            $scope.$on('socket:invitation', function(event, invitation){
                var host = UsersService.getUser(invitation.hostSocketId);
                if(confirm(host.name + ' wants to play with you'))
                    socket.emit('response', { hostSocketId: invitation.hostSocketId });
            });

            $scope.$on('socket:response', function(event, response){
                var guest = UsersService.getUser(response.guestSocketId);
                alert(guest.name + ' has agreed to play with you');
            });

            Object.defineProperty($scope, 'users', {
                get: function(){
                    return UsersService.users;
                }
            });
            $scope.promptName();
            $scope.reset();
        }]);
