angular.module('minesweeper')
    .directive('msGameRenderer', [
        function(){
            return {
                restrict: 'E',
                templateUrl: 'views/game-renderer.html',
                scope: {
                    game: '=',
                    reset: '=',
                    readOnly: '='
                },
                controller: [
                    '$scope',
                    'socket',
                    function($scope, socket){
                        (function setupUserActions(){
                            $scope.reveal = function(coordinates){
                                if($scope.readOnly)
                                    return;
                                var revealed = $scope.game.reveal(coordinates);
                                socket.emit('revealed', coordinates);
                            };

                            $scope.annotate = function(coordinates){
                                if($scope.readOnly)
                                    return;
                                var annotated = $scope.game.annotate(coordinates);
                                socket.emit('annotated', coordinates);
                            };
                        }());

                        (function setupScopeWatches(){
                            $scope.$watch('game', delayedTimerReset);
                            $scope.$watch('game.startTime', startTimer);
                            $scope.$watch('game.isWon()', stopTimerWhenGameWon);
                            $scope.$watch('game.lost', stopTimerWhenGameLost);

                            function delayedTimerReset(game){
                                if(game && !game.startTime)
                                    setTimeout(resetTimer, 0);

                                function resetTimer(){
                                    $scope.$broadcast('timer-reset');
                                    $scope.$digest();
                                }
                            }
                            function startTimer(startTime){
                                if(startTime)
                                    $scope.$broadcast('timer-start');
                            }
                            function stopTimerWhenGameWon(gameWon){
                                if(gameWon)
                                    $scope.$broadcast('timer-stop');
                            }
                            function stopTimerWhenGameLost(gameLost){
                                if(gameLost)
                                    $scope.$broadcast('timer-stop');
                            }
                        }());
                    }
                ]
            };
        }]);
